const router = require("express").Router();
const user = require("./user.router.js");
const cards = require("./card.router.js");



router.use("/users", user);
router.use("/cards", cards);



module.exports = router;