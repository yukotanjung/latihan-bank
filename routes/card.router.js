const router = require("express").Router();
const { body , validationResult, check  } = require('express-validator');
const Cards = require("../controllers/cards");
const card = new Cards();

router.get('/get-card-byid', (req,res) => {
    card.getCardById(req,res)
} )

router.post('/add-card', (req,res) => {
    card.addCard(req,res)
} )

module.exports = router;