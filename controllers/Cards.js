const model = require("../models");

class Cards {

    async getCardById(req,res){
        await model.Cards.findOne({
            where : {
                user_id : req.body.user_id
            }
          }
          )
            .then(function (result) {
                res.status(200).json({
                    status : 200,
                    data : result
                });
            })
            .catch(function (error) {
                res.status(500).json({ error: error });
            });
    }

        

    async addCard(req,res){
        
        await model.Cards.create({
              card_number: req.body.card_number,
              issuer: req.body.issuer,
              user_id: req.body.user_id,
              is_status: 't',
            })
              .then(function (result) {
                  res.status(200).json({
                      status : 200,
                      "data" : result
                  });
              })
              .catch(function (error) {
                  res.status(500).json({ error: error });
              });
      }

      generate(n) {
        var add = 1, max = 12 - add;   
        if ( n > max ) {
                return generate(max) + generate(n - max);
        }
        
        max        = Math.pow(10, n+add);
        var min    = max/10;
        var number = Math.floor( Math.random() * (max - min + 1) ) + min;
        
        return ("" + number).substring(add); 
    }
}

module.exports = Cards