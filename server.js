const express = require('express')
const app = express()
const port = 3000
const router = require("./routes/router.js");

app.use(express.urlencoded({ extended: true }));
app.use("/", router);


app.listen(port,() =>{
    console.log('running server with port ' + port);
})