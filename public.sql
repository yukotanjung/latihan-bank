/*
 Navicat Premium Data Transfer

 Source Server         : Local PG
 Source Server Type    : PostgreSQL
 Source Server Version : 140006
 Source Host           : localhost:5432
 Source Catalog        : marketplace
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140006
 File Encoding         : 65001

 Date: 02/02/2023 11:32:31
*/


-- ----------------------------
-- Sequence structure for Items_item_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Items_item_id_seq";
CREATE SEQUENCE "public"."Items_item_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for Orders_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Orders_id_seq";
CREATE SEQUENCE "public"."Orders_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for Users_userid_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Users_userid_seq";
CREATE SEQUENCE "public"."Users_userid_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for Items
-- ----------------------------
DROP TABLE IF EXISTS "public"."Items";
CREATE TABLE "public"."Items" (
  "item_id" int4 NOT NULL DEFAULT nextval('"Items_item_id_seq"'::regclass),
  "item_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "stock" int4 NOT NULL,
  "price" int4 NOT NULL,
  "status" int4 NOT NULL,
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Table structure for Orders
-- ----------------------------
DROP TABLE IF EXISTS "public"."Orders";
CREATE TABLE "public"."Orders" (
  "id" int4 NOT NULL DEFAULT nextval('"Orders_id_seq"'::regclass),
  "userid" int4 NOT NULL,
  "item_id" int4 NOT NULL,
  "qty" int4 NOT NULL,
  "price" int4 NOT NULL,
  "subtotal" int4 NOT NULL,
  "status" int4 NOT NULL,
  "createdAt" timestamptz(6) NOT NULL,
  "updatedAt" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Table structure for SequelizeMeta
-- ----------------------------
DROP TABLE IF EXISTS "public"."SequelizeMeta";
CREATE TABLE "public"."SequelizeMeta" (
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for Users
-- ----------------------------
DROP TABLE IF EXISTS "public"."Users";
CREATE TABLE "public"."Users" (
  "userid" int4 NOT NULL DEFAULT nextval('"Users_userid_seq"'::regclass),
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" text COLLATE "pg_catalog"."default" NOT NULL,
  "fullname" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "status" int4,
  "input_date" timestamptz(6) NOT NULL
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."Items_item_id_seq"
OWNED BY "public"."Items"."item_id";
SELECT setval('"public"."Items_item_id_seq"', 2, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."Orders_id_seq"
OWNED BY "public"."Orders"."id";
SELECT setval('"public"."Orders_id_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."Users_userid_seq"
OWNED BY "public"."Users"."userid";
SELECT setval('"public"."Users_userid_seq"', 8, true);

-- ----------------------------
-- Primary Key structure for table Items
-- ----------------------------
ALTER TABLE "public"."Items" ADD CONSTRAINT "Items_pkey" PRIMARY KEY ("item_id");

-- ----------------------------
-- Primary Key structure for table Orders
-- ----------------------------
ALTER TABLE "public"."Orders" ADD CONSTRAINT "Orders_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table SequelizeMeta
-- ----------------------------
ALTER TABLE "public"."SequelizeMeta" ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY ("name");

-- ----------------------------
-- Primary Key structure for table Users
-- ----------------------------
ALTER TABLE "public"."Users" ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("userid");
