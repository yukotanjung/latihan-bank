'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
   
     await queryInterface.bulkInsert('Users', [
      {
        name: 'John Doe',
        address: 'Jalan Merdeka 66',
        age : '27',
        job : 'UI/UX Designer',
        city : 'Bogor',
        email : 'john@gmail.com'
      },
      {
        name: 'Yuko',
        address: 'Jalan Kebangsaan Timur 77',
        age : '25',
        job : 'Backend Engineer',
        city : 'Malang',
        email : 'yuko@gmail.com'
      },
      {
        name: 'Timur',
        address: 'Jalan Surabaya 76',
        age : '23',
        job : 'Security',
        city : 'Depok',
        email : 'timur@gmail.com'
      },
      {
        name: 'Galang',
        address: 'Jalan Sulfat 12',
        age : '29',
        job : 'Manager',
        city : 'Tangerang',
        email : 'galang@gmail.com'
      },
      {
        name: 'Rambu Anarki',
        address: 'Jalan Paiton 32',
        age : '25',
        job : 'Product Owner',
        city : 'Bekasi',
        email : 'rambu@gmail.com'
      }
    
    ], {});
   
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
