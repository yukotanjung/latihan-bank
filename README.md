1. Run npm install to using this API
2. ERD design attachment in this API
3. API Documentation attachment in this API
4. Using Postman for open API Documentation
5. Create environment on Postman and create variable token

## How to using API
1. After you clone this API from gitlab run npm install
2. Make sure you already installed nodejs and postgree
3. Run sequelize db:migrate
4. Run npm run dev
5. Some endpoint need to login first
6. Create account if you dont have account yet
7. Hit endpoint of login and copy paste the response of token, write it into variable token on environment of Postman